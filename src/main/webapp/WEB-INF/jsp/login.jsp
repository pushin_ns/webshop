<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>

<head>
    <meta charset="utf-8" />
    <title>Логин</title>
    <link rel="stylesheet" type="text/css" href="/styles/auth-style.css">
</head>

<body>
    <form method="POST" action="login">
        <h1>Авторизация</h1>
        <input type="text" name="login" id="login" placeholder="Логин">
        <input type="password" name="password" id="password" placeholder="Пароль">
        <span id="error">${error}</span>

        <input type="submit" name="submit" id="submit-button" value="Вход">
        <a href="registration">Зарегистрироваться</a>
    </form>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/jquery.color-2.1.2.js"></script>
<script src="js/login-check.js"></script>
</html>