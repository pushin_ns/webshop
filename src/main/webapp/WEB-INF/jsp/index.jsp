<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>Товары</title>
<link rel="stylesheet" type="text/css" href="styles/style.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
</head>

<!--Заголовок-->
<body>
	<div id="header">
		<ul id="menu">
			<li id="username"><a href="login"><c:out value="${login}" /></a></li>
			<li id="logout"><a href="logout">Выход</a></li>
			<li id="cart"><a href="">Корзина <span id="cart-indicator"
					title="Товаров в корзине">
						<p id="numberOfItems">
							<c:if test="${empty numberOfItems}">0</c:if>
							<c:out value="${numberOfItems}" />
						</p>
				</span></a></li>
			<li id="search"><a href="">Поиск...</a></li>
		</ul>
	</div>

	<!--Окно поиска-->
	<div id="search-popup-container">
		<div id="search-popup">
			<h1>
				Поиск <img id="close-search"
					src="https://image.flaticon.com/icons/svg/359/359523.svg">
			</h1>
			<form>
				<input type="text" id="nameToFind" placeholder="Название товара..." />
  				<div id="minPrice">
  					<span class="text-span">Мин. цена: </span><input type="text" style="border:none" id="textInputMin" value=""><input type="range" name="minPrice" min="0" max="1000" step="1" onchange="updateTextInputMin(this.value);">
  				</div>
  				<div id="maxPrice">
  					<span class="text-span">Макс. цена: </span><input type="text" style="border:none" id="textInputMax" value=""><input type="range" name="maxPrice" min="0" max="1000" step="1" onchange="updateTextInputMax(this.value);">
 				</div>
				<input type="submit" id="searchBtn" value="Найти" />
				<script>
					function updateTextInputMax(val) {
				          document.getElementById('textInputMax').value=val;
				          
				        };
				    function updateTextInputMin(val) {
					      document.getElementById('textInputMin').value=val; 
					    };
				</script>
				<div id="search-results">
					<ul class="products">
						
					</ul>
					<ul id="pagination"></ul>
				</div>
			</form>
		</div>
	</div>
	<!--Корзина-->
	<div id="cart-popup-container">
		<div id="cart-popup">
			<h1>
				Корзина <img id="close-cart"
					src="https://image.flaticon.com/icons/svg/359/359523.svg">
			</h1>
			<div id="cart-container">
				<ul>
				<c:if test="${not empty cart}">
					<c:forEach items="${cart}" var="item">
							<li><img width="90" src='<c:out value="${item.imageUrl}" />'><span
								class="name"><c:out value="${item.name}" /></span><span
								class="price"><c:out value="${item.price}" /></span><span
								class="id" style="display: none"><c:out value="${item.id}" /></span></li>
						</c:forEach>
				</c:if>
					
				</ul>
			</div>
			<span id="sum-descr">Сумма товаров: </span><span id="sum"></span><br>
			<span id="discount-descr">Скидка: </span><span id="discount"><c:out
					value="${discount}" />%</span><br> <span id="final-sum-descr">Итого:
			</span><span id="final-sum"></span>
			<form action="toCart" method="post">
				<input type="submit" id="performBtn" value="Оформить" />
			</form>
		</div>
	</div>
	<!--Основной контент-->
	<div id="content">

		<!--Описание товара-->
		<div id="info-window"></div>

		<!-- Нажатие кнопки обрабатывается JS'ом -->
		<div id="toCartContainer">
			<span id="toCartBtn">В Корзину</span>
		</div>
		<!--Заголовок с категориями товаров-->
		<div id="sticky-header"></div>
		<div id="firstCat" class="category">
			<h1 class="fixed-header">Категория товаров 1</h1>
			<ul class="products">
				<c:forEach items="${products}" var="item">
					<li><img src='<c:out value="${item.imageUrl}" />'><span
						class="name"><c:out value="${item.name}" /></span><span
						class="price"><c:out value="${item.price}" /></span><span
						class="id" style="display: none"><c:out value="${item.id}" /></span></li>
				</c:forEach>
			</ul>
		</div>
		<div id="secondCat" class="category">
			<h1 class="fixed-header">Категория товаров 2</h1>
			<ul class="products">
				<li><img src="images/image.png"><span class="name">Товар1</span><span
					class="price">3116</span></li>

				<li><img src="images/orl.png"><span class="name">orlovets1</span><span
					class="price">5432</span></li>

				<li><img src="images/buldakov.png"><span class="name">buldakov1</span><span
					class="price">1000</span></li>

				<li><img src="images/image.jpg"><span class="name">Товар1</span><span
					class="price">1450</span></li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<a id="epam">© 2017, Internal Laboratory, EPAM Systems</a> <a
			href="https://vk.com/pushin_ns" id="initials">Pushin Nikita</a>
	</div>
	<script src="js/on-products-click-script.js"></script>
	<script src="js/search-click-listener.js"></script>
	<script src="js/cart-script.js"></script>
	<script src="js/jquery.twbsPagination.min.js"></script>

</body>

</html>