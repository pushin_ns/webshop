<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8" />
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="styles/auth-style.css">
</head>

<body>
    <form action="/register" method="POST">
        <h1>Регистрация</h1>
        <input type="text" name="firstName" class="reg-input-field" id="first-name" placeholder="Имя">
        <input type="text" name="lastName" class="reg-input-field" id="last-name" placeholder="Фамилия">
        <input type="text" name="login" class="reg-input-field" id="login" placeholder="Логин">
        <input type="password" name="password" class="reg-input-field" id="password" placeholder="Пароль">
        <input type="password" name="confirm-password" class="reg-input-field" id="confirm-password" placeholder="Повторить пароль">
        <span id="error">${error}</span>

        <input type="submit" name="submit" id="submit-button" value="Зарегистрироваться" />
        <a href="login">Авторизация</a>
    </form>

</body>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery.color-2.1.2.js"></script>
<script src="js/reg-check.js"></script>

</html>