$(document).ready(function () {
    $("#submit-button").click(function (e) {
        $('input').each(function () {
            if (($(this).val() === "") || ($(this).val().length <= 3)) {
                e.preventDefault();
                $('#error').html('Заполните все поля');
                $("#" + $(this).attr('id')).animate({ backgroundColor: '#ffd6d6' }, 500)
                    .animate({ backgroundColor: '#fff' }, 500);
            }
        });
    });
});