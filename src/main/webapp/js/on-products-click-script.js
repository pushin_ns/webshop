$(document).ready(function (event) {
    var item;
    $('.products li').click(function (e) {
        if ($(this).parent().parent().attr('id') != "search-results") {
            e.stopPropagation();
            item = $(this);
            var position = item.position();
            var top = position.top;

            /*Show infowindow */
            $("#info-window").empty();
            $("#info-window").animate({ "width": 0 }, 200, function () {
                $("#info-window").css({ "display": "block", "top": top + 7 });
                $("#info-window").animate({ "width": $("#content").width() - 30 }, 200, function () {
                    $("#info-window").html(item.html());
                    $("#info-window").append($("#toCartContainer").html());
                    $("#info-window .price").css({ "margin-right": "200px" });
                    $("#info-window .price").css({ "display": "block" });
                    $("#info-window .price").append(" руб.");

                    /*Blink the button by pressing */
                    $("#toCartBtn")
                        .mouseup(function () {
                            $("#toCartBtn").css({ "background-color": "#1bceaf" });
                        })
                        .mousedown(function () {
                            $("#toCartBtn").css({ "background-color": "#2bc09d" });
                        });
                });
            });
        }
    });

    /*Hiding info by doublecklicking */
    $("#info-window").dblclick(function (e) {
        if (e.target.id != "toCartBtn") {
            e.stopPropagation();
            hideInfo();
        }
    });
    $("html").click(function (e) {
        /*If target is toCartBtn perform an addition of clicked element to the cart*/
        if (e.target.id === 'toCartBtn') {
            e.stopPropagation();     
            var cartSize;
            var id = item.find(".id").html();
            $.ajax({
                type: 'get',
                url: 'cart/add/' + id,
                success: function(response){	
                	$("#cart-container ul").empty();
					if (!$.trim(response))
						$("#cart-container ul").append("<h2>Ошибка</h2>")
					else
					for (var i = 0, cartSize = len = response.length; i < len; i++){
						var item = response[i];
						$("#cart-container ul").append(
								"<li><img width='90' src='" + item.imageUrl + "'>" +
								"<span class='name'>" + item.name + "</span>" +
								"<span class='price'>" + item.price + " руб.</span>" +
								"<span	class='id' style='display: none'>" + item.id + "</span></li>");
					}
					$("#numberOfItems").html(cartSize);
                }
            });

        }
        /*Hiding info by clicking on other document */
        if (e.target.id != "info-window" && $(e.target).parent().attr("id") != "info-window") {
            e.stopPropagation();
            hideInfo();
        }
    });

    function hideInfo() {
        $("#info-window").empty();
        $("#info-window").animate({ "width": 0 }, 200, function () {
            $("#info-window").css("display", "none");
        });

    }
}); 
