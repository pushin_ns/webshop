$(document).ready(function () {

    $('#cart').click(function (e) {
        e.preventDefault();
        $('#close-cart').click(function (e) {
            hide(e);
        });
        $("#cart-container span:first-child").remove();

        //Lift window to top of document
        var body = $("html, body");
        body.stop().animate({ scrollTop: 0 }, 500, 'swing');
        //Show popup container
        $("#cart-popup-container").animate({ "width": "toggle" }, 100, function () {
            $(this).css('height', $(document).height());

            /*Indicates if cart is empty*/
            $("#cart-popup").animate({ "height": [ "toggle", "linear" ] }, 200);
            if (($("#cart-container").html()).replace(/[\t\n]+/g,"") == "<ul></ul>") 
            	showEmptyCart();
            
//            Show popup window.
            else {
                $("#cart-popup").children().each(function () {
                	$(this).show(300);
                });
                $("#cart-container .price").show();
                addMetaInformation();    
                deleteItem();
                
            }
            
        });

    });

    $('#cart-popup').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('#cart-popup-container').click(function (e) {
        hide(e);
    });
    
});
function showEmptyCart(){
	$("#cart-popup").children().each(function () {
        if ($(this).prop("tagName") != 'H1')
            $(this).hide();
    });
    $("#cart-container span:first-child").empty();
    $("#cart-container").prepend("<span>В корзине ничего нет</span>").show();

};
function deleteItem(){
	$('.delete-from-cart').click(function () {
    	var item = $(this).parent();
    	var id = item.find(".id").html();
    	$.ajax({
            type: 'get',
            url: 'cart/delete/' + id,
            success: function(response){
            	$("#cart-container ul").empty();
            	if (response.length == 0){
            		showEmptyCart();
            		$("#numberOfItems").html(0);
            	}
            	else {
	            	for (var i = 0, cartSize = len = response.length; i < len; i++){
						var item = response[i];
						$("#cart-container ul").append(
								"<li><img width='90' src='" + item.imageUrl + "'>" +
								"<span class='name'>" + item.name + "</span>" +
								"<span class='price'>" + item.price + " руб.</span>" +
								"<span	class='id' style='display: none'>" + item.id + "</span></li>");
					}
	                addMetaInformation();
	                deleteItem();
	            	$("#numberOfItems").html(cartSize);
            	}
            }
        });
    });
};
function addMetaInformation(){
	var sum = Number(0);
    $("#cart-container li").each(function () {
        sum += parseInt($(this).find(".price").text());
        $(this).append("<div class = 'delete-from-cart' title='Удалить из корзины'></div>");
    });
    $("#sum").html(sum).append(" руб.");
    var discount = parseInt($("#discount").html()) / 100;
    var finalSum = sum - sum * discount;
    $("#final-sum").html(Math.round(finalSum * 100) / 100).append(" руб.");
	$("#cart-popup").find(".price").show();
}

function hide(e) {
    $('html').css('overflow', 'auto');
    e.preventDefault();
    $("#cart-popup").hide(300);
    $("#cart-popup-container").hide(100);
    $("#cart-popup").css({ "opacity": "0" });
}
//Popup can't be placed outside of visible area
$(document).ready(function () {
    $('#cart-popup-container').on(
        'mousewheel', function (e) {
            if (e.originalEvent.wheelDelta / 120 > 0)
                return;
            if ($("#performBtn").offset().top > $(window).scrollTop() + 700) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
        }
    )
});