$(document).ready(function () {
    $('#search').click(function (e) {
        e.preventDefault();
        $("#search-popup-container").animate({ "width": "toggle" }, 200, function () {
            $(this).css("display", "block");
            $(this).css('height', $(document).height());
            $("#search-popup").animate({ "height": "toggle", "opacity": 2 }, 200, function () {
                $(this).css("display", "block");
            });
            $('#close-search').click(function (e) {
                hide(e);
            });
        });

    });
    $('#search-popup').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('#searchBtn').click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		var data = $(this).parent().find("#nameToFind").val();
		var minPrice = $(this).parent().find("#textInputMin").val();
		var maxPrice = $(this).parent().find("#textInputMax").val();
		if (maxPrice != "" && minPrice != "")
			data = data + '/' + minPrice + '/' + maxPrice
			$("#search-results .products").empty();
		$('#pagination').empty();
		$.ajax({
				type : 'get',
				url : 'find/' + data,
				success : function(response) {
					$('#pagination').twbsPagination('destroy');
					if (!$.trim(response)){
						$("#search-results .products").append("<h2>Ничего не найдено</h2>");
					}
					else{
						var itemsOnPage = 15;
						var totalPages = Math.ceil(response.length / itemsOnPage);
						$('#pagination').twbsPagination({
							  totalPages: totalPages,
							  visiblePages: totalPages,
							  onPageClick: function (event, page) {
								  $(".active").css("background-color","#1bceaf");
								  $("#search-results .products").empty();
								  for (var i = page * itemsOnPage - itemsOnPage; i < page * itemsOnPage; i++){
										var item = response[i];
										if (typeof item !== "undefined")
											$("#search-results .products").append(
												"<li><img width='90' src='" + item.imageUrl + "'>" +
												"<span class='name'>" + item.name + "</span>" +
												"<span class='price'>" + item.price + " руб.</span>" +
												"<span	class='id' style='display: none'>" + item.id + "</span></li>");
										else{
											$("#search-popup").find(".price").show();
											return;
										}				
									}
									$("#search-popup").find(".price").show();
							  }
							});
					}
				}
		});
	});
    $('#search-popup-container').click(function (e) {
        hide(e);
    });
    function hide(e) {
        $('html').css('overflow', 'auto');
        e.preventDefault();
        $("#search-popup").hide(200);
        $("#search-popup-container").hide(200);
        $("#search-popup").css({ "opacity": "0" });
    }
});
