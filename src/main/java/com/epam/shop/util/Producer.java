//package shop.util;
//
//import java.util.List;
//import java.util.concurrent.CopyOnWriteArrayList;
//
//import org.springframework.stereotype.Component;
//
//import shop.domain.Product;
//import shop.domain.User;
//
//@Component
//public class Producer {
//
//	private List<Product> productList;
//	private List<Product> cart = new CopyOnWriteArrayList<>();
//	private List<User> usersList;
//	private final static long TEST_USER_ID = 123456;
//
//	public Producer() {
//		this.productList = new CopyOnWriteArrayList<Product>();
//		this.usersList = new CopyOnWriteArrayList<User>();
//		String imageUrl = "images/cargo-512.png";
//		for (int i = 0; i < 50; i++) {
//			productList.add(new Product(i, "Товар " + i, (int)Math.round(Math.floor(Math.random() * 1000)), "ddd", imageUrl));
//		}
//		for (int i = 0; i < 20; i++) {
//			usersList.add(new User());
//		}
//		usersList.add(new User(TEST_USER_ID));
//	}
//
//	public void addToCart(Product product) {
//		this.cart.add(product);
//	}
//
//	public void deleteFromUsers() {
//
//	}
//
//	public void deleteFromProducts() {
//
//	}
//
//	public void updateProducts(Product p) {
//
//	}
//
//	public void updateUsers(User u) {
//
//	}
//
//	public void addToProducts(Product product) {
//		this.productList.add(product);
//	}
//
//	public void addToUsers(User user) {
//		this.usersList.add(user);
//	}
//
//	public void setCart(List<Product> cart) {
//		this.cart = cart;
//	}
//
//	public List<Product> getCart() {
//		return cart;
//	}
//
//	public List<Product> getProductList() {
//		return productList;
//	}
//
//	public void setProductList(List<Product> productList) {
//		this.productList = productList;
//	}
//
//	public List<User> getUsersList() {
//		return usersList;
//	}
//
//	public void setUsersList(List<User> usersList) {
//		this.usersList = usersList;
//	}
//
//}
