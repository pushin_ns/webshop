package shop.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import shop.manager.UserSessionManager;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	UserSessionManager userSessionManager;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (userSessionManager.getUser() == null) {
			response.sendRedirect("/notLogged");
			return false;
		}
		return true;
	}

}
