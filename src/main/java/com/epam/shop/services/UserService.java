package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.UsersRepository;
import shop.dao.impl.UsersRepositoryImpl;
import shop.domain.User;
import shop.exception.InvalidPasswordException;
import shop.exception.NoSuchUserException;
import shop.exception.UserAlreadyExistsException;

@Service
public class UserService {
	@Autowired
	UsersRepository usersRepository;

	public UserService(UsersRepositoryImpl usersDAO) {
		this.usersRepository = usersDAO;
	}


	@Transactional
	public void createNewUser(User user) throws UserAlreadyExistsException {
		if (usersRepository.getByLogin(user.getLogin()) != null)
			throw new UserAlreadyExistsException();
		usersRepository.createOne(user);
	}

	public User getUserById(long id) {
		return usersRepository.getById(id);
	}

	public User getUserByLogin(String login) throws NoSuchUserException {
		User user = usersRepository.getByLogin(login);
		if (user == null)
			throw new NoSuchUserException();
		else
			return user;
	}

	public User authenticate(String login, String password) throws NoSuchUserException, InvalidPasswordException {
		User user = getUserByLogin(login);
		if (!user.getPassword().equals(password))
			throw new InvalidPasswordException();
		return user;
	}
}
