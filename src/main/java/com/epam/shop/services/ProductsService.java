package shop.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import shop.dao.ProductsRepository;
import shop.dao.impl.ProductsRepositoryImpl;
import shop.domain.Product;

@Service
@Transactional
public class ProductsService {

	ProductsRepository productsRepository;

	@Autowired
	public ProductsService(ProductsRepositoryImpl productDao) {
		this.productsRepository = productDao;
	}

	public void addToCart(long id) {
		productsRepository.addToCart(getProductById(id));

	}

	public void deleteFromCart(long id) {
		productsRepository.deleteFromCart(id);
	}

	public Set<Product> getCart() {
		System.out.println("service");
		return productsRepository.getCart();

	}

	public List<Product> getAllProducts() {
		return productsRepository.getAll();
	}

	public Product getProductById(long id) {
		return productsRepository.getById(id);
	}

	public List<Product> findProductsByNameAndPrice(String nameToFind, double minPrice, double maxPrice) {
		List<Product> searchResults = new ArrayList<>();
		for (Product product : getAllProducts()) {
			double price = product.getPrice();
			if (product.getName().toLowerCase().contains(nameToFind.toLowerCase()) && price >= minPrice
					&& price <= maxPrice)
				searchResults.add(product);
		}
		return searchResults;
	}

	public List<Product> findProductsByName(String nameToFind) {
		List<Product> searchResults = new ArrayList<>();
		for (Product product : getAllProducts()) {
			if (product.getName().toLowerCase().contains(nameToFind.toLowerCase()))
				searchResults.add(product);
		}
		return searchResults;
	}
}
