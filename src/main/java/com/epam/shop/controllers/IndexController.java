package shop.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import shop.domain.Product;
import shop.manager.UserSessionManager;
import shop.services.ProductsService;

@Controller
public class IndexController {

	@Autowired
	ProductsService productsService;
	@Autowired
	UserSessionManager userSessionManager;

	@GetMapping({ "/", "/index" })
	public String index(Model model) {
		List<Product> products = productsService.getAllProducts();
		model.addAttribute("products", products);
		model.addAttribute("login", userSessionManager.getUser().getLogin());
		try {
			Set<Product> cart = productsService.getCart();
			System.out.println(cart);
			model.addAttribute("numberOfItems", cart.size());
			if (!cart.isEmpty()) {
				model.addAttribute("cart", cart);
			}
		} catch (Exception e) {
			System.out.println("eee");
		}

		model.addAttribute("discount", Math.round(Math.random() * 50));
		return "index";
	}
}
