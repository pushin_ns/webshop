package shop.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import shop.domain.User;
import shop.exception.UserAlreadyExistsException;
import shop.manager.UserSessionManager;
import shop.services.UserService;

@Controller
public class RegistrationController {

	@Autowired
	UserService userService;
	@Autowired
	UserSessionManager userSessionManager;

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

	@GetMapping("/registration")
	public String getRegPage() {
		return "registration";
	}

	@PostMapping("/register")
	public String register(@RequestParam("login") String login, @RequestParam("password") String password,
			@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName)
			throws UserAlreadyExistsException {
		User user = new User(login, password, firstName, lastName);
		userService.createNewUser(user);
		LOGGER.info("Created new {}. Redirecting to /index", user);
		userSessionManager.setUser(user);
		return "redirect:/index";
	}

	@ExceptionHandler(UserAlreadyExistsException.class)
	public String handleUserAlreadyExistsException(Model model) {
		model.addAttribute("error", "Логин занят");
		return "registration";
	}
}
