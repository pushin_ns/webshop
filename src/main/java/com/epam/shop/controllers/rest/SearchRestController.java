package shop.controllers.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import shop.domain.Product;
import shop.services.ProductsService;

@RestController
@RequestMapping("/find")
public class SearchRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchRestController.class);
	@Autowired
	ProductsService productsService;

	@GetMapping("/")
	public List<Product> findProductsByEmptyName() {
		return findProductsByName("");
	}

	@GetMapping("/{nameToFind}")
	public List<Product> findProductsByName(@PathVariable String nameToFind) {
		LOGGER.info("Find products with name = '{}'", nameToFind);
		return productsService.findProductsByName(nameToFind);
	}

	@GetMapping("//{minPrice}/{maxPrice}")
	public List<Product> findProductsByPrice(@PathVariable double minPrice, @PathVariable double maxPrice) {
		return findProductsByNameAndPrice("", minPrice, maxPrice);
	}

	@GetMapping("/{nameToFind}/{minPrice}/{maxPrice}")
	public List<Product> findProductsByNameAndPrice(@PathVariable String nameToFind, @PathVariable double minPrice,
			@PathVariable double maxPrice) {
		return productsService.findProductsByNameAndPrice(nameToFind, minPrice, maxPrice);
	}

}
