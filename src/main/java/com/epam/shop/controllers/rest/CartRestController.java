package shop.controllers.rest;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import shop.domain.Product;
import shop.services.ProductsService;

@RestController
@RequestMapping("/cart/")
public class CartRestController {

	@Autowired
	ProductsService productsService;

	@GetMapping("add/{id}")
	public Set<Product> addToCartById(@PathVariable long id, Model model) {
		productsService.addToCart(id);
		System.out.println("controller");
		return productsService.getCart();
	}

	@GetMapping("delete/{id}")
	public Set<Product> deleteFromCart(@PathVariable long id, Model model) {
		productsService.deleteFromCart(id);
		return productsService.getCart();
	}
}
