package shop.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import shop.domain.User;
import shop.exception.InvalidPasswordException;
import shop.exception.NoSuchUserException;
import shop.exception.NotAuthenticatedException;
import shop.manager.UserSessionManager;
import shop.services.UserService;

@Controller
public class AuthController {
	@Autowired
	UserService userService;
	@Autowired
	UserSessionManager userSessionManager;
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	@GetMapping("login")
	public String getLoginPage() {
		LOGGER.info("Accessing page 'login'");
		return "login";
	}

	@PostMapping("login")
	public String authenticate(@RequestParam("login") String login, @RequestParam("password") String password,
			Model model) throws NoSuchUserException, InvalidPasswordException {
		User user = userService.authenticate(login, password);
		LOGGER.info("User - {}", user);
		userSessionManager.setUser(user);
		return "redirect:/index";
	}

	@GetMapping("logout")
	public String logOut(HttpSession session) {
		session.invalidate();
		return "login";
	}

	@GetMapping("notLogged")
	public String getNeedLoginPAge(Model model) {
		LOGGER.info("Redirected to page 'login'");
		model.addAttribute("error", "Авторизуйтесь для получения доступа");
		return "login";
	}

	@ExceptionHandler(NotAuthenticatedException.class)
	public String handleNotAuthenticatedException(Model model) {
		model.addAttribute("error", "Авторизуйтесь, чтобы получить доступ");
		return "login";
	}

	@ExceptionHandler(NoSuchUserException.class)
	public String handleNoSuchUserException(Model model) {
		model.addAttribute("error", "Пользователя с таким логином не существует");
		return "login";
	}

	@ExceptionHandler(InvalidPasswordException.class)
	public String handleInvalidPasswordException(Model model) {
		model.addAttribute("error", "Неправильный пароль");
		return "login";
	}
}
