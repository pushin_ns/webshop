package shop.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.UsersRepository;
import shop.domain.User;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

	@PersistenceContext
	EntityManager entityManager;

	@Transactional
	@Override
	public void createOne(User obj) {
		entityManager.persist(obj);

	}

	@Override
	public void deleteOne(User obj) {
	}

	@Override
	public User getById(long id) {
		return entityManager.find(User.class, id);
	}

	@Override
	public void deleteById(long id) {
	}

	@Override
	public void updateOne(User obj) {
		// producer.updateUsers(obj);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAll() {
		return entityManager.createQuery("SELECT p FROM User p").getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public User getByLogin(String login) {
		Session session = entityManager.unwrap(Session.class);
		List<User> users = session.createCriteria(User.class).add(Restrictions.eq("login", login)).list();
		if (users.isEmpty())
			return null;
		return users.get(0);
	}

}
