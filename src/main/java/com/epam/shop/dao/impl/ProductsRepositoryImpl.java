package shop.dao.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import shop.dao.ProductsRepository;
import shop.domain.Order;
import shop.domain.Product;
import shop.domain.User;
import shop.manager.UserSessionManager;

@Repository
@Transactional
public class ProductsRepositoryImpl implements ProductsRepository {

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	UserSessionManager userSessionManager;

	@Override
	public void createOne(Product obj) {
		// producer.addToProducts(obj);
	}

	@Override
	public void deleteOne(Product obj) {
		// producer.deleteFromProducts();

	}

	@Override
	public Product getById(long id) {
		return entityManager.find(Product.class, id);
	}

	@Override
	public void deleteById(long id) {
		// producer.deleteFromProducts();
	}

	@Override
	public void updateOne(Product obj) {
		// producer.updateProducts(obj);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAll() {
		return entityManager.createQuery("SELECT p FROM Product p").getResultList();
	}

	@Override
	public void addToCart(Product p) {
		Order order = new Order();
		order.setProductId(p.getId());
		order.setUserId(userSessionManager.getUser().getId());
		entityManager.persist(order);
	}

	@Override
	public Set<Product> getCart() {
		User user = entityManager.find(User.class, userSessionManager.getUser().getId());
		Set<Product> products = user.getProducts();
		System.out.println("repo");
		System.out.println(products);
		System.out.println(user.getProducts());
		return products;
	}

	@Override
	public void deleteFromCart(long id) {
		Set<Product> products = getCart();
		for (Product product : products) {
			if (product.getId() == id) {
				products.remove(product);
				return;
			}
		}
	}
}
