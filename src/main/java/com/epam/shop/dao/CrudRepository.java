package shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface CrudRepository<T> {

	public void createOne(T obj);

	public void deleteOne(T obj);

	public T getById(long id);

	public void deleteById(long id);

	public void updateOne(T obj);

	public List<T> getAll();

}
