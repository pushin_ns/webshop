package shop.dao;

import org.springframework.stereotype.Repository;

import shop.domain.User;

@Repository
public interface UsersRepository extends CrudRepository<User> {

	public User getByLogin(String login);
}
