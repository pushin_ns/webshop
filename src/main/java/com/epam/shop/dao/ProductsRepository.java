package shop.dao;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import shop.domain.Product;
//import shop.domain.Product1;

@Repository
public interface ProductsRepository extends CrudRepository<Product>{
	
	void addToCart(Product p);
	
	Set<Product> getCart();
	
	void deleteFromCart(long id);
	
}
