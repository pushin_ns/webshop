package shop.manager;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.context.annotation.ScopedProxyMode;

import shop.domain.User;

@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class UserSessionManager {

	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}